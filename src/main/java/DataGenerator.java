import com.github.vincentrussell.json.datagenerator.functions.FunctionRegistry;
import generators.ICustomGenerator;
import generators.RelationshipCollector;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOCase;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.reflections.Reflections;

import java.io.*;
import java.util.Arrays;
import java.util.Set;


public class DataGenerator {

    public static final String BASE_DIR = System.getProperty("user.dir");
    public static final String SCHEMA_DIR_PATH = BASE_DIR + "/src/main/schemas/";
    public static final String TARGET_DIR_PATH = BASE_DIR + "/src/main/generated/";

    public static void main(String[] args) {

        System.out.println("Data Generator for Darwin/EvoBench\n");

        // Auto-register all generators implementing the ICustomGenerator interface

        try {

            Reflections reflections = new Reflections("generators");
            Set<Class<? extends ICustomGenerator>> classes = reflections.getSubTypesOf(ICustomGenerator.class);

            for (Class generator : classes) {
                FunctionRegistry functionRegistry = FunctionRegistry.getInstance();
                functionRegistry.registerClass(generator);
                System.out.println("Custom generators: Registered " + generator.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Scan schema folder for schema files to process


        File schemaDirectory = new File(SCHEMA_DIR_PATH);
        FileFilter fileFilter = new WildcardFileFilter("*.schema", IOCase.INSENSITIVE);
        File[] listOfSchemas = schemaDirectory.listFiles(fileFilter);
        Arrays.sort(listOfSchemas);

        try {
            for (File schema : listOfSchemas) {
                System.out.println("Found schema at " + schema.toString());
            }

            // Create analysis wrapper for each schema file

            for (File schema : listOfSchemas) {

                // Get schema filename, e.g. Customer.schema.bak, remove extension and add .json as extension for the output
                String outputFileName = FilenameUtils.removeExtension(schema.getName()) + ".json";
                File outputFile = new File(TARGET_DIR_PATH + outputFileName);

                EntityTypeGenerator generator = new EntityTypeGenerator(schema, outputFile);
                generator.generateEntitiesToFile();

                // Emulate optional properties by removing the NOPROPERTY string

                System.out.println(" Starting preprocessing generated file");

                File cleanedFile = new File(TARGET_DIR_PATH + outputFileName + ".tmp");
                try {
                    BufferedReader reader = new BufferedReader(new FileReader(outputFile));
                    BufferedWriter writer = new BufferedWriter(new FileWriter(cleanedFile));

                    int optionalPropertyOccurences = 0;
                    final long timeStart = System.currentTimeMillis();

                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        if (!line.trim().equals("\"NOPROPERTY\",")) {
                            writer.write(line);
                            writer.newLine();
                        } else {
                            ++optionalPropertyOccurences;
                        }
                    }
                    reader.close();
                    writer.close();

                    if(outputFile.delete()) {
                        cleanedFile.renameTo(outputFile);
                    }

                    final long timeEnd = System.currentTimeMillis();

                    System.out.println("Done. Found " + optionalPropertyOccurences + " optional properties (" + (timeEnd-timeStart) + "ms)");

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (NullPointerException e) {
            System.out.println("No schema files to process, exiting");
            System.exit(0);
        }
    }
}
