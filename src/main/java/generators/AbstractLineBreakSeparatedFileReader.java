package generators;

import org.atteo.classindex.IndexAnnotated;
import org.atteo.classindex.IndexSubclasses;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

abstract class AbstractLineBreakSeparatedFileReader implements ItemReader {

    public String readItem(File filepath) {

        try {

            String result = null;
            Random rand = new Random();
            int n = 0;
            for(Scanner sc = new Scanner(filepath); sc.hasNext(); )
            {
                ++n;
                String line = sc.nextLine();
                if(rand.nextInt(n) == 0)
                    result = line;
            }

            return result;


        } catch (FileNotFoundException e) {
            return "[!] Dictionary Not Found";
        }

    };
}
