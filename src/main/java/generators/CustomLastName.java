package generators;

import com.github.vincentrussell.json.datagenerator.functions.Function;
import com.github.vincentrussell.json.datagenerator.functions.FunctionInvocation;

import java.io.File;

@Function(name = "customLastName")
public class CustomLastName extends AbstractLineBreakSeparatedFileReader implements ICustomGenerator {

    private static final File DICTIONARY = new File("src/main/dictionaries/lastnames.txt");

    @FunctionInvocation
    public String getCustomLastName() {

        String lastName = super.readItem(DICTIONARY);
        lastName = lastName.substring(0,1).toUpperCase() + lastName.substring(1);
        return lastName;
    }
}
