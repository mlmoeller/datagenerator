package generators;

import com.github.vincentrussell.json.datagenerator.functions.Function;
import com.github.vincentrussell.json.datagenerator.functions.FunctionInvocation;

import java.io.File;

@Function(name = "customFirstName")
public class CustomFirstName extends AbstractLineBreakSeparatedFileReader implements ICustomGenerator {

    private static final File DICTIONARY = new File("src/main/dictionaries/firstnames.txt");

    @FunctionInvocation
    public String getCustomFirstName() {

        String firstName = super.readItem(DICTIONARY);
        firstName = firstName.substring(0,1).toUpperCase() + firstName.substring(1);
        return firstName;
    }
}
