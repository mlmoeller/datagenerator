package generators;

import java.io.File;

public interface ItemReader {

    String readItem(File filepath);
}
