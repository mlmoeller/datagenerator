package generators;

import com.github.vincentrussell.json.datagenerator.functions.Function;
import com.github.vincentrussell.json.datagenerator.functions.FunctionInvocation;

import java.io.File;
import java.io.IOError;
import java.util.EmptyStackException;
import java.util.Random;

@Function(name = "genderWithProbability")
public class GenderWithProbability implements ICustomGenerator {

    @FunctionInvocation
    public String getGenderWithProbalitity(String pprobability) throws EmptyStackException {

        try {
            int probability = Integer.parseInt(pprobability);

            if (new Random().nextInt(probability) == 0) {
                if (new Random().nextInt() % 2== 0) {
                    // Notice: Start and end brackets are created by the generator parser
                    return "gender\": \"male";
                }
                return "gender\": \"female";
            }

        } catch (NumberFormatException e) {
            return "NOPROPERTY";
        }

        return "NOPROPERTY";
    }
}
