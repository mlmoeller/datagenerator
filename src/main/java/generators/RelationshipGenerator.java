package generators;

import com.github.vincentrussell.json.datagenerator.functions.Function;
import com.github.vincentrussell.json.datagenerator.functions.FunctionInvocation;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;

@Function(name = "relationship")
public class RelationshipGenerator implements ICustomGenerator {

    private String relationshipName;
    private int relationshipIdentifier;
    private HashSet<Integer> observedIdentifiers;

    public RelationshipGenerator() {
        this.relationshipIdentifier = 0;
        this.observedIdentifiers = new HashSet<Integer>();
    }

    @FunctionInvocation
    public String findOrCreateRelationship(String relationshipName, String command, String startingPoint) {

        // Check if the String "command" represents a value in the scope of the RelationshipCommand enum

        RelationshipCommand parsedCommand;

        try {
            parsedCommand = RelationshipCommand.valueOf(command);
        } catch (IllegalArgumentException e) {
            parsedCommand = RelationshipCommand.REPEAT;
        }

        // Check if startingPoint is an actual numbers, if not set to 0

        int parsedStartingPoint;

        try {
            parsedStartingPoint = Integer.parseInt(startingPoint);
        } catch (NumberFormatException e) {
            parsedStartingPoint = 0;
        }

        // Check all relationships if a relationship with the name "relationshipName" was yet defined
        // If so, reuse it (reuses a relationship)
        // If not, it is implicitly created (created a relationship)

        RelationshipGenerator concreteRelationship = RelationshipCollector.findOrCreateRelationship(relationshipName);


        switch (parsedCommand){
            case INIT:
                concreteRelationship.relationshipIdentifier = 0;
                return "";
            case INCREMENT:
                concreteRelationship.incrementRelationshipValue();
                return Integer.toString(concreteRelationship.getRelationShipIdentifier());
            case INITTOVALUE:
                concreteRelationship.setRelationshipIdentifier(parsedStartingPoint);
                concreteRelationship.incrementRelationshipValue();
                return Integer.toString(concreteRelationship.getRelationShipIdentifier());
            case RANDOM:
                return Integer.toString(concreteRelationship.getRandomRelationshipIdentifier());
            case REPEAT:
            default:
                return Integer.toString(concreteRelationship.getRelationShipIdentifier());
        }
    }

    private void incrementRelationshipValue() {
        ++relationshipIdentifier;
        observedIdentifiers.add(relationshipIdentifier);
    }

    public void setRelationshipName(String name) {
        relationshipName = name;
    }

    public String getRelationshipName() {
        return relationshipName;
    }

    public int getRelationShipIdentifier() {
        return relationshipIdentifier;
    }

    // gets a random idenfier based on the observed scope
    public int getRandomRelationshipIdentifier() {
        Random random = new Random();
        int index = random.nextInt(observedIdentifiers.size());

        Iterator<Integer> iterator = observedIdentifiers.iterator();
        for (int i=0; i<index; i++) {
            iterator.next();
        }

        return iterator.next();
    }

    private void setRelationshipIdentifier(int startingPoint) {
        relationshipIdentifier = startingPoint;
        observedIdentifiers.add(relationshipIdentifier);
    }
}
